import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_web_socket/main.dart';

class HomePage extends StatelessWidget{
  String token;

  @override
  Widget build(BuildContext context) => new Scaffold(
    appBar: new AppBar(
      title: new Text('Home'),
    ),
    body: new Container(
      margin: new EdgeInsets.only(
          top: 50.0
      ),
      alignment: Alignment.center,
      child: new Column(
        children: <Widget>[
          new Text('Welcome to App!'),
          //new Text(auth.token),
          new FlatButton(
            child: new Text(
                'Logout'
            ),
            onPressed: () {
              auth.logout().then(
                      (_) => Navigator.of(context).pushReplacementNamed('/login')
              );
            }
        )
        ],
      ),
    ),
  );
}