import 'package:http/http.dart' as http;
import 'package:stomp/stomp.dart';
import 'dart:convert';
import 'package:web_socket_channel/io.dart';
import 'stompService.dart' as stomp;


class Response{
  String result;
  Object payload;
  String securityToken;

  Response({this.result, this.payload, this.securityToken});

  factory Response.fromJson(Map<String, dynamic> json){
    return Response(
      result: json['result'],
      payload: json['payload'],
      securityToken: json['securityToken']
    );
  }
}

class SocketService{
  StompClient _client;
  String _serverUrl;
  String _sendUrl;
  String _receiveUrl;

  SocketService(this._serverUrl, this._sendUrl, this._receiveUrl);

  Future<StompClient> connect(token) async {
    if(token == null)
      throw new Exception('Auth-Token is required');

    return await stomp.connectWith(
        new IOWebSocketChannel.connect(_serverUrl), login: token)
    .catchError((data) => throw new Exception("Error: $data")
    )
    .then((StompClient client){
      _client = client;
    });
  }

  void sendSomeMessage(someData){
    if(_client == null)
      throw new Exception('StompClient is null');

    if(someData == null)
      throw new Exception('Message is null');

    _client.sendJson(_sendUrl, someData);
  }

  void subscribe(callback){
    if(_client == null)
      throw new Exception('StompClient is null');

    _client.subscribeString("0", _receiveUrl, (headers, message) => callback(message));
  }
}

class AuthService{
  String token = '';

  Future<String> getToken() async{
    return this.token;
  }

  Future<bool> logout() async {
    this.token = '';
  }

  Future<Response> login(String userName, String pass) async {
    final resp = await http.post(
        "https://staff-eng.com/server/login",
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          'email':userName,
          'password':pass
        }));

    return Response.fromJson(json.decode(resp.body));
  }
}