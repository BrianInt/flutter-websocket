import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_web_socket/main.dart';

class LoginPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{
  BuildContext context;
  final _userTextController = TextEditingController();
  final _passTextController = TextEditingController();
  final _loginForm = GlobalKey<FormState>();

  String error = '';

  void login(){
    auth.login(_userTextController.text, _passTextController.text).then((result){
      if(result.result != 'OK') {
        this.error = result.payload;
        return;
      }
      auth.token = result.securityToken;

      socket.connect(result.securityToken).then((data){
        socket.subscribe((result) => {
          print("Result $result")
        });

        socket.sendSomeMessage({"name": "funkisl13@gmail.com", "username": "Hi, WebSocket runs on the Flutter"});
      });

      Navigator.of(context).pushReplacementNamed('/home');
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Scaffold(
      appBar: AppBar(
          title: new Text('Login')
      ),
      body: Center(
          child: Container(
            margin: EdgeInsets.all(50.0),
            width: 300.0,
            child: Column(
              children: <Widget>[
                Text(error, textAlign: TextAlign.center,style: TextStyle(color: Colors.redAccent)),
                Form(
                  key: _loginForm,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextFormField(
                          validator: (value){
                            if(value.isEmpty)
                              return 'Please enter email';
                          },
                          decoration: InputDecoration(
                              labelText: 'Email'
                          ),
                          controller: _userTextController,
                        ),
                        TextFormField(
                          validator: (value){
                            if(value.isEmpty)
                              return 'Please enter password';
                          },
                          decoration: InputDecoration(
                              labelText: 'Password'
                          ),
                          controller: _passTextController,
                        ),
                      ],
                    )),
                Container(
                  margin: EdgeInsets.only(top: 20.0),
                  width: 300.0,
                  child: RaisedButton(
                      onPressed: (){
                        if(_loginForm.currentState.validate()){
                          login();
                        }
                      },
                      child: Text("Login")
                  ),
                )
              ],
            ),
          )
      ),
    );
  }
}