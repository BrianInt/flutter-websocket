// This sample shows adding an action to an [AppBar] that opens a shopping cart.

import 'package:flutter/material.dart';
import 'package:test_web_socket/home.page.dart';
import 'package:test_web_socket/login.page.dart';
import 'package:test_web_socket/service/auth.service.dart';

AuthService auth = new AuthService();
SocketService socket = new SocketService(
    "ws://192.168.1.195:7434/commonNotifications/websocket",
    "/app/send/message",
    "/user/queue/messages");

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget _defaultHome = new LoginPage();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.AppBar.actions',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: _defaultHome,
      routes: <String, WidgetBuilder>{
        '/home':(BuildContext context) => new HomePage(),
        '/login':(BuildContext context) => new LoginPage(),
      }
    );
  }
}


